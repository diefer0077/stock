<?php require_once '../header.php'; ?>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
        <h3>productos</h3>
        <table class="table table-bordered table-responsive">    
            <form method="post" action="add.php"> 
            <tr><td><b>Descripcion</td><td><input type="text" name="descripcion"></td></tr>
            <tr><td><b>Stock mínimo</td><td><input type="text" name="stock_minimo"></td></tr>
            <tr><td><b>Stock máximo</td><td><input type="text" name="stock_maximo"></td></tr>
            <tr><td></td><td><input class="btn btn-primary" name="enviar" type="submit" value="Cadastrar">&nbsp;&nbsp;&nbsp;
            <input class="btn btn-warning" name="enviar" type="button" onclick="location='index.php'" value="Voltar"></td></tr>
            </form>
        </table>
        </div>
    </div>
</div>

<?php

if(isset($_POST['enviar'])){
    $descripcion = $_POST['descripcion'];
    $stock_minimo = $_POST['stock_minimo'];
    $stock_maximo = $_POST['stock_maximo'];

    require_once('../conexion.php');
    try{
       $sql = "INSERT INTO productos(descripcion,stock_minimo,stock_maximo) VALUES (?, ?, ?)";
       $stm = $pdo->prepare($sql)->execute([$descripcion, $stock_minimo, $stock_maximo]);;
 
       if($stm){
           echo 'Dados inseridos com sucesso';
     header('location: index.php');
       }
       else{
           echo 'Erro ao inserir os dados';
       }
   }
   catch(PDOException $e){
      echo $e->getMessage();
   }
}
require_once('../footer.php');
?>
