<?php
include_once("../conexion.php");

if (isset($_POST["page"])) {
    $page_no = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
    if(!is_numeric($page_no))
        die("Error fetching data! Invalid page number!!!");
} else {
    $page_no = 1;
}

// get record starting position
$start = (($page_no-1) * $row_limit);

if($sgbd == 'mysql'){
    $results = $pdo->prepare("SELECT id, producto_id, cantidad, precio, data FROM compras ORDER BY id LIMIT $start, $row_limit");
}elseif($sgbd == 'pgsql'){
    $results = $pdo->prepare("SELECT id, producto_id, cantidad, precio, data FROM compras ORDER BY id LIMIT $row_limit OFFSET $start");    
}
$results->execute();

while($row = $results->fetch(PDO::FETCH_ASSOC)) {
// Trazer a Descripcion do producto atual
$sql = 'select descripcion from productos where id = '.$row['producto_id'];
$stm = $pdo->query($sql);
$producto = $stm->fetch(PDO::FETCH_OBJ);

    echo "<tr>" . 
    "<td>" . $row['id'] . "</td>" . 
    "<td>" . $producto->descripcion . "</td>".
    "<td>" . $row['cantidad'] . "</td>".
    "<td>" . $row['precio'] . "</td>".
    "<td>" . $row['data'] . "</td>
    </tr>";
}
